# CREATED BY: Marcus Edward Buter
# STARTED: 09:38pm 09/07/2022
# FINISHED: 
# DESCRIPTION: A curses application to continue using live install or install to a drive.


import curses, os

run = True

def draw(stdscr, size, menus, boot):

    
    rows = size[0]-1
    cols = size[1]-3
    cenRows = int(rows/2)
    cenCols = int(cols/2)

    #stdscr.addstr(str(menus['main']['run']))
    if main:
        # Main Screen
        description_size = (7, 61)
        description_window = curses.newwin(description_size[0], description_size[1], int(cenRows - description_size[0]/2)+8, int(cenCols - description_size[1]/2))
        #shadow_window = curses.newwin(35, 70, 5, 5)

        stdscr.addstr(0, 0, "ButlerOS INSTALLATION\n═════════════════════════")
        stdscr.bkgd(' ', curses.color_pair(1))

        # Logo Amc Razor
        logo1 = [
        "       _    ___         ___  ___   ___                   ___        __  ",
        "  .'|=| `. |   | |`.   `._|=|   |=|_.'   .'|        .'|=|_.'   .'|=|  | ",
        ".'  | | .' |   | |  `.      |   |      .'  |      .'  |  ___ .'  | |  | ",
        "|   |=|'.  |   | |   |      |   |      |   |      |   |=|_.' |   |=|.'  ",
        "|   | |  | `.  | |   |      `.  |      |   |  ___ |   |  ___ |   |  |`. ",
        "|___|=|_.'   `.|=|___|        `.|      |___|=|_.' |___|=|_.' |___|  |_| ",
        ]
        logo2 = [
        "             ___   ___ ",
        "  .'|=|`.   |   |=|_.' ",
        ".'  | |  `. `.  |      ",
        "|   | |   |   `.|=|`.  ",
        "`.  | |  .'  ___  |  `.",
        "  `.|=|.'    `._|=|___|",
        ]

        index = 0
        for line in logo1:
            stdscr.addstr(20+index, int(cenCols-95/2), line)
            index += 1
        index = 0
        for line in logo2:
            stdscr.addstr(20+index, int(cenCols-95/2)+72, line, curses.color_pair(3))
            index += 1

        # Print out what bios mode.
        stdscr.addstr(int(cenRows-len(str(boot))/2), int(cenCols-len(str(boot))-11/2), f"BIOS MODE: {boot}")

        #stdscr.addstr(5, 5, str(key))
        # CHOICES
        padding = 5
        if menus['main']['selection'] == 1:
            stdscr.addstr(cenRows, int(cenCols-61/2), "Live/Rescue/Demo Operating System (Command Line)", curses.A_REVERSE)
            description = "Boots into live/rescue/demo operating system command line. No graphical interface. Does NOT SCREW with any drives. Any changes made, files edited or created, any programs downloaded will NOT be saved after a reboot. Type \"install\" in any directory to come back to this menu."
        else:
            stdscr.addstr(cenRows, int(cenCols-61/2), "Live/Rescue/Demo Operating System (Command Line)")
        if menus['main']['selection'] == 2:
            stdscr.addstr(cenRows+1, int(cenCols-61/2), "Live/Rescue/Demo Operating System (Graphical User Interface)", curses.A_REVERSE)
            description = "Boots into live/rescue/demo operating system with a graphical user interface. LeftWM as window manager. Does NOT SCREW with any drives. Any changes made, files edited or created, any programs downloaded will NOT be saved after a reboot. Click \"install\" in the bar to come back to this menu."
        else:
            stdscr.addstr(cenRows+1, int(cenCols-61/2), "Live/Rescue/Demo Operating System (Graphical User Interface)")
        if menus['main']['selection'] == 3:
            stdscr.addstr(cenRows+2, int(cenCols-61/2), "Install Operating System", curses.A_REVERSE)
            description = "Install Operating System to a drive. Don't forget to backup files you want to keep. :)"
        else:
            stdscr.addstr(cenRows+2, int(cenCols-61/2), "Install Operating System")

        # Description
        # Clean up description so words are left on one line.
        # Add /n character to end of words so the whole word is on one line.
        # EXAMPLE:
        # Hello Wo
        # rld
        # GOES TO:
        # Hello World
        # Can use this code for a word editor.
        size = 0
        index = 0
        old_description = description.split(' ')
        description = []
        while index < len(old_description):
            size += len(old_description[index])+1
            description.append(old_description[index] + " ")
            if size > description_size[1]:
                description.pop()
                description[-1] = description[-1].replace(" ", "")
                #description.append("\b" + description[-1])
                # Deletes space at end of line causing \n character to shift down a line in curses.
                description.append("\n")
                size = 0
                index -= 1

            index += 1
        # DEBUG
        #stdscr.addstr(5, 0, str(description))
        description = "".join(description)

        description_window.addstr(0, 0, description, curses.color_pair(2))

        # DRAW TO SCREEN
        stdscr.refresh()
        description_window.refresh()
        # shadow_window.refresh()


def update(menus, key):
    """ Interprete key presses. """
    global run
    for menu in menus.keys():
        if menus[menu]['run']:
           if key == "KEY_UP":
               menus[menu]['selection']-= 1
           elif key == "KEY_DOWN":
               menus[menu]['selection']+= 1
           if menus[menu]['selection'] == 0:
               menus[menu]['selection']= menus[menu]['choices']
           elif menus[menu]['selection'] == menus[menu]['choices'] + 1:
               menus[menu]['selection']= 1

    if key == "\n":
        # LiveOS cmd
        if menus['main']['selection'] == 1:
            run = False
        # LiveOS GUI
        elif menus['main']['selection'] == 2:
            pass
        # Install OS
        else:
            pass


    return menus

def main(stdscr):
    global run
    size = stdscr.getmaxyx()

    curses.noecho()
    curses.curs_set(0)
    stdscr.keypad(True)
    curses.start_color()
    # COLOR PAIRS
    curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_RED)
    curses.init_pair(2, curses.COLOR_WHITE, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_RED)

    key = None

    menus = {"main": {"run": True, "choices": 3, "selection": 1}}

    # Booted with BIOS or UEFI
    boot = os.system("[ -d /sys/firmware/efi ] && echo UEFI || echo BIOS")
    if boot == 0:
        boot = "UEFI"
    else:
        boot = "BIOS"
    #run = True
    while run:
        # Clear screen
        #stdscr.refresh()
        stdscr.erase()
        menus = update(menus, key)
        draw(stdscr, size, menus, boot)
        #stdscr.addstr(5, 5, str(menus))
        #stdscr.addstr(6, 5, str(menus['main']['run']))
        key = stdscr.getkey()

curses.wrapper(main)

if menus['main']['selection']:
    print("Type \"install\" to get back to this menu.")
