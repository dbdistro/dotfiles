#!/usr/bin/env python3

import psutil
from termcolor import colored
from colorama import Fore


def is_process_running(process_name):
    for process in psutil.process_iter():
        if process.name() == process_name:
            return True
    return False

if is_process_running("borg"):
    print("\nBacking up...")

else:
    print("\n...")
