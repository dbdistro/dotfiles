//  _        ___                                      ___ _
// | |      / __)_                                   / __|_)
// | | ____| |__| |_ _ _ _ ____      ____ ___  ____ | |__ _  ____    ____ ___  ____
// | |/ _  )  __)  _) | | |    \    / ___) _ \|  _ \|  __) |/ _  |  / ___) _ \|  _ \
// | ( (/ /| |  | |_| | | | | | |  ( (__| |_| | | | | |  | ( ( | |_| |  | |_| | | | |
// |_|\____)_|   \___)____|_|_|_|   \____)___/|_| |_|_|  |_|\_|| (_)_|   \___/|_| |_|
// A WindowManager for Adventurers                         (____/
// For info about configuration please visit https://github.com/leftwm/leftwm/wiki

#![enable(implicit_some)]
(
    modkey: "Mod4",
    mousekey: "Mod4",
    workspaces: [],
    tags: [
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
    ],
    max_window_width: None,
    layouts: [
        "MainAndVertStack",
        "MainAndHorizontalStack",
        "MainAndDeck",
        "Grid",
        "EvenHorizontal",
        "EvenVertical",
        "Fibonacci",
        "CenterMain",
        "CenterMainBalanced",
        "Monocle",
        "RightWiderLeftStack",
        "LeftWiderRightStack",
    ],
    layout_mode: Tag,
    insert_behavior: Bottom,
    scratchpad: [    
    	( name: "terminal", // This is the name which is referenced when calling (case-sensitive)
    	  value: "kitty ~", // The command to load the application if it isn't started (first application to start)
	      // x, y, width, height are in pixels when an integer is inputted or a percentage when a float is inputted.
	      // These values are relative to the size of the workspace, and will be restricted depending on the workspace size.
    	),
    	(name: "passwords", value: "keepassxc"),
    ],
    window_rules: [
    	(window_class: "mainf.*", spawn_floating: true),
    	(window_title: "Qalculate!", spawn_floating: true),
    	(window_class: "lxpolkit", spawn_floating: true),
    	
    ],
    disable_current_tag_swap: false,
    disable_tile_drag: false,
    disable_window_snap: true,
    focus_behaviour: Sloppy,
    focus_new_windows: true,
    single_window_border: true,
    sloppy_mouse_follows_focus: true,
    auto_derive_workspaces: true,
    keybind: [
		(command: Execute, value: "/opt/monitor_layout.sh", modifier: ["modkey"], key: "s"),
        (command: SetLayout, value: "Monocle", modifier: ["modkey", "Shift"], key: "m"),
        (command: SetLayout, value: "MainAndVertStack", modifier: ["modkey", "Shift"], key: "s"),
        (command: SetLayout, value: "Grid", modifier: ["modkey", "Shift"], key: "h"),
        (command: Execute, value: "rofi -show drun -display-drun \'Run\' -scroll-method 1 -location 0 & mpv $HOME/.config/dbD/system_sounds/menu.mp3", modifier: ["modkey"], key: "r"),
        (command: Execute, value: "rofi -show power-menu -modi \'power-menu:~/.config/rofi/scripts/rofi-power-menu-master/rofi-power-menu --choices=lockscreen/suspend/logout/shutdown/reboot\'", modifier: ["modkey"], key: "x"),
        (command: Execute, value: "rofi -modi \"clipboard:greenclip print\" -show clipboard -run-command '{cmd}'", modifier: ["modkey"], key: "c"),
        (command: Execute, value: "$HOME/.config/rofi/scripts/rofi-music/rofi-music", modifier: ["modkey"], key: "m"),
		(command: Execute, value: "kitty -1", modifier: ["modkey"], key: "Return"),
        (command: CloseWindow, value: "", modifier: ["modkey", "Shift"], key: "q"),
        (command: SoftReload, value: "", modifier: ["modkey", "Shift"], key: "r"),
        (command: MoveWindowUp, value: "", modifier: ["modkey", "Shift"], key: "k"),
        (command: MoveWindowDown, value: "", modifier: ["modkey", "Shift"], key: "j"),
        (command: MoveWindowUp, value: "", modifier: ["modkey", "Shift"], key: "Up"),
        (command: MoveWindowDown, value: "", modifier: ["modkey", "Shift"], key: "Down"),
        (command: FocusWindowUp, value: "", modifier: ["modkey"], key: "Up"),
        (command: FocusWindowUp, value: "", modifier: ["modkey"], key: "k"),
        (command: FocusWindowDown, value: "", modifier: ["modkey"], key: "Down"),
        (command: FocusWindowDown, value: "", modifier: ["modkey"], key: "j"),
        (command: IncreaseMainWidth, value: "5", modifier: ["modkey", "Alt"], key: "l"),
        (command: DecreaseMainWidth, value: "5", modifier: ["modkey", "Alt"], key: "h"),
        (command: FocusWorkspaceNext, value: "", modifier: ["modkey"], key: "l"),
        (command: FocusWorkspacePrevious, value: "", modifier: ["modkey"], key: "h"),
        (command: NextLayout, value: "", modifier: ["modkey", "Control"], key: "Up"),
        (command: PreviousLayout, value: "", modifier: ["modkey", "Control"], key: "Down"),
		(command: ToggleFloating, modifier: ["modkey", "Control"], key: "f"),
        (command: GotoTag, value: "1", modifier: ["modkey"], key: "1"),
        (command: GotoTag, value: "2", modifier: ["modkey"], key: "2"),
        (command: GotoTag, value: "3", modifier: ["modkey"], key: "3"),
        (command: GotoTag, value: "4", modifier: ["modkey"], key: "4"),
        (command: GotoTag, value: "5", modifier: ["modkey"], key: "5"),
        (command: GotoTag, value: "6", modifier: ["modkey"], key: "6"),
        (command: MoveToTag, value: "1", modifier: ["modkey", "Shift"], key: "1"),
        (command: MoveToTag, value: "2", modifier: ["modkey", "Shift"], key: "2"),
        (command: MoveToTag, value: "3", modifier: ["modkey", "Shift"], key: "3"),
        (command: MoveToTag, value: "4", modifier: ["modkey", "Shift"], key: "4"),
        (command: MoveToTag, value: "5", modifier: ["modkey", "Shift"], key: "5"),
        (command: MoveToTag, value: "6", modifier: ["modkey", "Shift"], key: "6"),
        (command: Execute, value: "pactl -- set-sink-volume 0 +5%", modifier: ["modkey"], key: "equal"),
        (command: Execute, value: "pactl -- set-sink-volume 0 -5%", modifier: ["modkey"], key: "minus"),
        (command: Execute, value: "pactl set-sink-mute 0 toggle", modifier: ["modkey"], key: "space"),
		(command: ToggleScratchPad,	value: "terminal" , modifier: ["modkey"], key: "f"),
		(command: ToggleScratchPad,	value: "passwords" , modifier: ["modkey"], key: "p"),
    ],
    state_path: None,
)
