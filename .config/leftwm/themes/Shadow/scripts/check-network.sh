# CREATED BY: Marcus Edward Butler
# CREATION DATE: 08/24/2022
# REVISED: 2023/05/11
# DESCRIPTION: A polybar module/widget that tells you if you are connected to the internet.

#!/bin/bash

wget -q --tries=10 --timeout=5 --spider https://archlinux.org
if [[ $? -eq 0 ]]; then
	echo "%{F#0f0}Online%{F-}"
else
	echo "%{F#f00}Offline%{F-}"
fi
