#!/usr/bin/env python3

"""
NAME: Marcus Edward Butler
REVISION: 0 2024/04/05

DESCRIPTION: A polybar widget that displays drive space. Automatically detects
             internal drives.
             EXAMPLE: root 101.84 GB / 463.64 GB home 200.72 GB / 900 GB
"""

import psutil, humanize
from termcolor import cprint

# Colors
grey = "\033[037m"
end = "\033[0m"


# Get mountpoints - Checks if drive exists
disk_partitions = psutil.disk_partitions(all=False)

mountpoints = []
for disk in disk_partitions:
    mountpoints.append(disk.mountpoint)


if '/' in mountpoints: 
    # Root Drive usage/size
    root_drive = psutil.disk_usage('/')
    cprint('/', 'dark_grey', end=' ') 
    print(humanize.naturalsize(root_drive.used, binary=True, gnu=False).replace('i', ''), '-', humanize.naturalsize(root_drive.total, binary=True, gnu=False).replace('i', ''), end='  ')

if '/home' in mountpoints:
    # Home Drive usage/size
    home_drive = psutil.disk_usage('/home')
    cprint('/home' 'dark_grey', end=' ')
    print(humanize.naturalsize(home_drive.used, binary=True, gnu=False).replace('i', ''), '-', humanize.naturalsize(home_drive.total, binary=True, gnu=False).replace('i', ''))


print()
