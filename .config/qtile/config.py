# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
# Copyright (c) 2024-2025 Marcus Ed. Butler
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from libqtile import bar, layout, qtile, widget, hook
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen, ScratchPad, DropDown
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

from qtile_extras import widget
from qtile_extras.widget.decorations import PowerLineDecoration

import os, subprocess
from random import randint

from scripts.random_wallpaper import random_wallpaper, random_wallpaper_keybind



"""
WIDGETS
"""
# Adds used space u.
class DF_mod(widget.DF):
    def __init__(self, **args):
        super().__init__(**args)

    def poll(self):
        statvfs = os.statvfs(self.partition)

        size = statvfs.f_frsize * statvfs.f_blocks // self.calc
        free = statvfs.f_frsize * statvfs.f_bfree // self.calc
        self.user_free = statvfs.f_frsize * statvfs.f_bavail // self.calc

        if self.visible_on_warn and self.user_free >= self.warn_space:
            text = ""
        else:
            text = self.format.format(
                p=self.partition,
                s=size,
                f=free,
                uf=self.user_free,
                m=self.measure,
                r=(size - self.user_free) / size * 100,
                u=size - self.user_free,
            )

        return text

@hook.subscribe.client_new
def window_rules(window):
    if window.name == "American Truck Simulator":
        window.togroup("2")
        window.cmd_toggle_fullscreen()
        window.cmd_center()
        window.cmd_bring_to_front()
        widnow.cmd_focus()
        

"""
Start up scripts
"""
# Set Resolution
subprocess.run("xrandr --output HDMI1 --auto --output DP1 --auto --output DP1 --left-of HDMI1 &", shell=True)

# Run compositor
subprocess.run("picom --config $HOME/.config/picom/picom.conf &", shell=True)

# Set Background
random_wallpaper()

# Startup Sound
subprocess.run("mpv $HOME/.config/dbD/system_sounds/startup.ogg &", shell=True)

# Lock Screen
subprocess.run("xss-lock --transfer-sleep-lock -- $HOME/.bin/lock --nofork &", shell=True)

# num Lock
subprocess.run("numlockx on &", shell=True)

# sudo GUI
subprocess.run("lxsession &", shell=True)

# Notifications
subprocess.run("dunst &", shell=True)

# Clipboard Manager
subprocess.run("greenclip daemon &", shell=True)

home_dir = os.path.expanduser('~')

mod = "mod4"
terminal = guess_terminal()

### WINDOW GROUP / SCRATCH PADS #######################################################

# groups = [Group(i) for i in "1234567890"]

groups = [
    ## SCRATCH PADS ##
    ScratchPad("scratchpad", [
    
    # KeepassXC
    DropDown("passwords", "keepassxc", on_focus_lost_hide=False, x=0.3, width=0.2, opacity=1.0),
    DropDown("cmd line", "kitty", on_focus_lost_hide=False, x=0.54, y=0.59, width=0.45, opacity=1.0),
    DropDown("file manager", "kitty --hold sh -c yazi", on_focus_lost_hide=False, x=0.54, y=0.01, width=0.45, opacity=1.0),

    ]),

    Group("1"),
    Group("2"),
    Group("3"),
    Group("4"),
    Group("5"),
    Group("6"),
    Group("7"),
    Group("8"),
    Group("9"),
    

]


### KEY BINDS ##########################################################
keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "m", lazy.next_screen(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn("kitty"), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key(
        [mod],
        "f",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen on the focused window",
    ),
    Key([mod], "f", lazy.window.toggle_floating(), desc="Toggle floating on the focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),

    # Change wallpaper
    Key([mod], "w", lazy.spawn(f"{home_dir}/.config/rofi/scripts/rofi-wallpaper/rofi-wallpaper"), desc="Change the Wallpaper"),
    

    # Terminal Scratchpad
    Key([mod], "t", lazy.group['scratchpad'].dropdown_toggle("cmd line")),

    # File Manager
    # Key([mod], "f", lazy.group['scratchpad'].dropdown_toggle("file manager")),

    # Change the volume.
    Key([mod], "a", lazy.spawn('pactl -- set-sink-volume 0 +5%'), desc="Increase the volume"),
    Key([mod], "s", lazy.spawn('pactl -- set-sink-volume 0 -5%'), desc="Decrease the volume"),
    Key([mod], "space", lazy.spawn('pactl set-sink-mute 0 toggle'), desc="Toggle muting the volume"),

    # Run Prompts
    KeyChord([mod], "r", [
        Key([], "r", lazy.spawn("rofi -show drun -display-drun \'Run\' -scroll-method 1 -location 0")),
        Key([], "w", lazy.spawn("rofi -show window -scroll-method 1 -location 0")),
        Key([], "x", lazy.spawn(f"{home_dir}/.config/rofi/scripts/rofi-power-menu-master/pre-run.sh")),
        Key([], "c", lazy.spawn("rofi -modi \"clipboard:greenclip print\" -show clipboard -scroll-method 1 -location 0 -run-command '{cmd}'")),
        Key([], "p", lazy.group['scratchpad'].dropdown_toggle('passwords'))
    ]),

    # Layout Switcher
    KeyChord([mod], "d", [
        Key([], "m", lazy.group.setlayout('max')),
        Key([], "c", lazy.group.setlayout('columns')),
        Key([], "t", lazy.group.setlayout('treetab')),
        Key([], "f", lazy.group.setlayout('floating')),
        Key([], "p", lazy.group.setlayout('plasma')),
        Key([], "space", lazy.next_layout()),
    ]),
]

# Add key bindings to switch VTs in Wayland.
# We can't check qtile.core.name in default config as it is loaded before qtile is started
# We therefore defer the check until the key binding is run by using .when(func=...)
for vt in range(1, 8):
    keys.append(
        Key(
            ["control", "mod1"],
            f"f{vt}",
            lazy.core.change_vt(vt).when(func=lambda: qtile.core.name == "wayland"),
            desc=f"Switch to VT{vt}",
        )
    )

for i in groups:
    if i.name != "scratchpad":
        keys.extend(
            [
                # mod1 + group number = switch to group
                Key(
                    [mod],
                    i.name,
                    lazy.group[i.name].toscreen(),
                    desc="Switch to group {}".format(i.name),
                ),
                # mod1 + shift + group number = switch to & move focused window to group
                Key(
                    [mod, "shift"],
                    i.name,
                    lazy.window.togroup(i.name, switch_group=False),
                    desc="Switch to & move focused window to group {}".format(i.name),
                ),
                # Or, use below if you prefer not to switch to that group.
                # # mod1 + shift + group number = move focused window to group
                # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
                #     desc="move focused window to group {}".format(i.name)),
            ]
        )



layout_theme = {
    "border_focus": "#00a800", 
    "border_normal": "#b50000", 
    "border_on_single": True, 
    "border_width": 4,
    "margin": 0,
}
bar_theme = {
    "background_1": "#e7ff00",
    "background_2": "#e7ff00",
    "font_color": "",
}

### LAYOUTS ##############################################
layouts = [
    layout.Columns(**layout_theme),
    layout.Floating(**layout_theme),
    layout.Max(**layout_theme),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2)
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    layout.Plasma(**layout_theme),
    # layout.RatioTile(),
    # layout.Slice(**layout_theme),
    # layout.Tile(**layout_theme),
    layout.TreeTab(**layout_theme,
        sections=["Tree Tab"],
        bg_color="282626",
        font="RobotoMonoNerdFont",
        active_bg="#ff0000",
        fontsize=12,
        padding_left=0,
    ),
    # layout.VerticalTile(**layout_theme),
    # layout.Zoomy(**layout_theme),
]



widget_defaults = dict(
    # font="TerminessNerdFont",
    # font="SymbolsNerdFont",
    font="RobotoMonoNerdFont",
    fontsize=12,
    padding=2,
)
extension_defaults = widget_defaults.copy()


def get_connected_monitors():
    output = subprocess.check_output(["xrandr", "--listactivemonitors"]).decode("utf-8")
    monitors = output[10]
    return monitors


powerline = {
    "decorations": [
        PowerLineDecoration(path="forward_slash"),
    ]
}

### BAR  #######################################
taskbar = [
    # Battery widget.
    widget.CapsNumLockIndicator(
        fontsize=10,
        **powerline,
    ),
    widget.Volume(
        background = "#ff0000",
        fmt="Vol: {}",
        mute_format="Mute",
        **powerline,
    ),
    widget.Memory(
        format = "{MemUsed:.2f}MB/{MemTotal:.2f}MB",
        **powerline,
    ),
    widget.CPU(
        background = "#ff0000",
        format = "{freq_current}GHz {load_percent}%",
        **powerline,

    ),
    # Storage goes.
    widget.Net(
        format='{down:.0f}{down_suffix} ↓↑ {up:.0f}{up_suffix}',
        background="#ff0000",
        **powerline,
        ),
    widget.GenPollText(update_interval=1, func=lambda: subprocess.check_output(os.path.expanduser("~/.config/dbD/bar/widgets/check-borg.py"), shell=True, text=True)),
    widget.Spacer(),
    widget.Spacer(),
    widget.Clock(
        format="%Y-%m-%d %a %I:%M %p",
        **powerline,
    ),
    widget.WindowName(
        background = "#ff0000",
        for_current_screen = True,
        **powerline,
    ),
    widget.GroupBox(
        highlight_method="block",
        active = "#ffffff",
        foreground = "#ff0000",
        block_highlight_text_color = "#ffffff",

        other_current_screen_border = "#00a800",
        other_screen_border = "#b50000",
        this_current_screen_border = "#00a800",
        this_screen_border = "#b50000",        
    ),
]
# Add drive storage amount, figure out if home dir is seperate from root dir.
# Is /home dir mounted?
with open ("/etc/fstab", 'r') as f_obj:
    fstab_content = f_obj.read()

if "/home" in fstab_content:
    # Root dir without powerline.
    taskbar.insert(4, DF_mod(
        update_interval = 60,
        partition = "/",
        format = "/:{u}{m}/{s}{m}",
        visible_on_warn = False,
        ),
    )
    taskbar.insert(5,
        DF_mod(
            update_interval = 60,
            partition = "/home",
            format = "/home:{u}{m}/{s}{m}",
            visible_on_warn = False,
            **powerline,

        ),   
    )
    
else:
    # Root dir with powerline.
    taskbar.insert(4, DF_mod(
        update_interval = 60,
        partition = "/",
        format = "/:{u}{m}/{s}{m}",
        visible_on_warn = False,
        **powerline,
        ),
    )

# Check if tower or laptop. (Adds battery widget)
if len(os.listdir("/sys/class/power_supply")) > 0:
    # Laptop
    taskbar.insert(0, widget.Battery(
        background = "#ff0000",
        format = "{char}{percent:2.0%} {hour:d}:{min:02d} {watt:.2f}W",
        not_charging_char = "DC",
        **powerline,
    ))

# Create a bar for each monitor.
screens = []
for monitor in range(int(get_connected_monitors())):
    screens.append(Screen(top=bar.Bar(taskbar, 24, background="#282626")))

# # Init monitors
# def init_screens():
#     return [Screen(top=bar.Bar()),
#             Screen(top=bar.Bar())]


# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = True
floating_layout = layout.Floating(
    **layout_theme,

    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,

        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ],

)

auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
