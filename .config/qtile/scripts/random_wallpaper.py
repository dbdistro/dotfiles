# Copyright (c) 2024 Marcus Ed. Butler
# VERSION: 2024-10-19_R0
# DESCRIPTION: When ran picks a random wallpaper from the specified directory uses feh.

import os, subprocess
from random import randint
from libqtile.lazy import lazy


def random_wallpaper():
    # Pick random background.
    ## Get home directory.
    wallpaper_path = os.path.join(os.path.expanduser('~'), "w/wallpapers/wallpapers")

    ## Pick a random wallpaper
    wallpapers = os.listdir(wallpaper_path)
    wallpaper_number = randint(0, len(wallpapers) - 1)
    wallpaper = wallpapers[wallpaper_number]

    ## join the wallpaper path with the chosen wallpaper.
    chosen_wallpaper = os.path.join(wallpaper_path, wallpaper)

    ## Use feh to draw the wallpaper.
    subprocess.run("killall feh", shell=True)
    subprocess.run(f"feh --bg-fill \"{chosen_wallpaper}\" &", shell=True)


# Set Background "decorator"
@lazy.function
def random_wallpaper_keybind(qtile):
    random_wallpaper()
