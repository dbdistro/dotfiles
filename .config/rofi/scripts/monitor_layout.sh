# Created: Marcus Butler
# Date Started: 01/04/2022
# Finished: 01/04/2022
# DESCRIPTION: Uses rofi to let you choose your monitor layout.


#!/bin/bash


internal_monitor="eDP1"
external_monitor="HDMI1"

# Path to up and down scripts.
config_path=~/.config/leftwm/themes/current 
echo $config_path


# Reset polybar(taskbar program) and feh(background program)
function restart { 
	/bin/bash "$config_path/down" 
	/bin/bash "$config_path/up"
}


# Uses rofi to create a menu of options.
selected=$(echo -e "disconnect\nexpand\nclone\nprojector" | rofi -dmenu -p "Monitor Layout")
echo $selected


# Disconnect
if [ $selected = "disconnect" ]
then
	xrandr --output $internal_monitor --auto --output $external_monitor --off
	restart
	exit 0
fi


# Check if there is an external monitor.
monitors=$(xrandr | grep " connected " | awk '{ print$1 }')
echo $monitors

# Count how many monitors
monitor_number=$(wc -w <<< "$monitors")

# Exits script and returns error if only 1 monitor
if [ $monitor_number -lt 2 ]
then
	rofi -e "No Monitor Detected"
	exit 0
fi


xrandr --output "HDMI1" --set audio force-dvi --auto

# Check the option the user selected.
# Clone
if [ $selected = "clone" ]
then
	xrandr --output $internal_monitor --off --output $external_monitor --auto
fi

# Projector
if [ $selected = "projector" ]
then
	xrandr --output $internal_monitor --auto --output $external_monitor --auto
fi 

# Expand
if [ $selected = "expand" ]
then
	# Only works if I clone first, I don't know why.
	xrandr --output $internal_monitor --off --output $external_monitor --auto
	xrandr --output $internal_monitor --auto --output $external_monitor --left-of $internal_monitor
fi

restart
