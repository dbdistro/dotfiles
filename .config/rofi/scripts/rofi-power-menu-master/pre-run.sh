#!/usr/bin/env bash

# Check if system is backing up and warn user.
if pgrep borg > /dev/null; then
	rofi -e "System is backing up.";
fi

rofi -show power-menu -scroll-method 1 -location 0 -modi 'power-menu:~/.config/rofi/scripts/rofi-power-menu-master/rofi-power-menu --choices=lockscreen/suspend/logout/shutdown/reboot'

