# dbDistro Dotfiles

**NOTE**: Please do not run these commands unless you know what they do.

**NOTE-2:** If you would like to manage your own dotfiles repo, just fork this project.

**NOTE-3:** Link to orginal page I learned to do this with.<br><br> 

[Dotfiles: Best way to store in a bare git repository](https://www.atlassian.com/git/tutorials/dotfiles)
<br><br>

## Install

1) Log out then drop to a tty before running these commands. Running these in a GUI could cause a system crash.<br>
`CTRL` + `ALT` + `F3`

2) Delete folders `.config` `.zshrc` and `.bin`.<br>
`rm -rf .config .zshrc .bin`

1) Create an alias<br>
`alias dotfiles='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'`

2) Ignore the source repo.<br>
`echo ".cfg" >> .gitignore`

3) Clone this repo into a local **bare** repo. _(First link is a perma link)_<br>
`git clone --bare http://projects.distrobutler.com/dotfiles $HOME/.cfg`<br>or<br>
`git clone --bare https://gitlab.com/dbdistro/dotfiles $HOME/.cfg`

4) Check out the repo.<br>
`dotfiles checkout`

5) Set showUntrackedFiles to no.<br>
`dotfiles config --local status.showUntrackedFiles no`


## Update/Add Dotfile(s)
`dotfiles add <FILE/DIR_NAME>`

## Remove Dotfile(s) without deleting files on local machine.
`dotfiles rm --cached <FILE/DIR_NAME>`
